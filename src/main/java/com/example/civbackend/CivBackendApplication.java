package com.example.civbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CivBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(CivBackendApplication.class, args);
    }
}
