package com.example.civbackend.Post;

import com.example.civbackend.Post.Entity.Like.LikeRequestDto;
import com.example.civbackend.Post.Entity.PostEntity;
import com.example.civbackend.Post.Entity.PostRequestDto;
import com.example.civbackend.Post.Entity.PostResponseDto;

import java.io.IOException;
import java.util.ArrayList;

public interface PostService {
    PostResponseDto createPost(PostRequestDto rq, String name) throws IOException;
    PostResponseDto likePost(LikeRequestDto rq, String name);


    void saveLike(Long userId, PostEntity post, boolean like);

    PostResponseDto getPost(Long id);

    ArrayList<PostResponseDto> getPosts();

    byte[] getPhoto(Long id);

    PostResponseDto deletePost(Long id, String username) throws ClassNotFoundException;

    ArrayList<PostResponseDto> getPostsByUser(Long id);
}
