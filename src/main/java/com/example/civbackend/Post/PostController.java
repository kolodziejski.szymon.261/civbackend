package com.example.civbackend.Post;

import com.example.civbackend.Post.Entity.Like.LikeRequestDto;
import com.example.civbackend.Post.Entity.PhotoResponseDto;
import com.example.civbackend.Post.Entity.PostRequestDto;
import com.example.civbackend.Post.Entity.PostResponseDto;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;

@RequiredArgsConstructor
@RestController
@Validated
public class PostController {
    private final PostService service;

    @GetMapping(path = "/posts")
    public ResponseEntity<ArrayList<PostResponseDto>> getPosts() {
        return new ResponseEntity<>(this.service.getPosts(), HttpStatus.OK);
    }

    @PostMapping(path = "/posts/add", consumes = "multipart/form-data")
    public ResponseEntity<PostResponseDto> createPost(final @Valid @ModelAttribute PostRequestDto postRequestDto) throws IOException {
        return new ResponseEntity<>(
                this.service.createPost(postRequestDto, SecurityContextHolder.getContext().getAuthentication().getName()),
                HttpStatus.CREATED);
    }

    @PostMapping(path = "/posts/like")
    public ResponseEntity<PostResponseDto> likePost(final @Valid @RequestBody LikeRequestDto likeRequestDto) {
        PostResponseDto p = this.service.likePost(likeRequestDto, SecurityContextHolder.getContext().getAuthentication().getName());
        if (p == null) return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(p,
                HttpStatus.CREATED);
    }
    @GetMapping(path = "/posts/photo/{id}")
    public ResponseEntity<PhotoResponseDto> getPhoto(@PathVariable String id) {
        byte[] photo = this.service.getPhoto(Long.valueOf(id));
        if (photo == null) return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        String s = Base64.getEncoder().encodeToString(photo);
        PhotoResponseDto p = new PhotoResponseDto(s);
        return new ResponseEntity<>(p, HttpStatus.OK);
    }

    @GetMapping("/posts/user/{id}")
    public ResponseEntity<ArrayList<PostResponseDto>> getPostsByUser(@PathVariable String id) {
        return new ResponseEntity<>(this.service.getPostsByUser(Long.valueOf(id)), HttpStatus.OK);
    }

    @GetMapping(path = "/posts/{id}")
    public ResponseEntity<PostResponseDto> getPost(@PathVariable String id) {
        PostResponseDto p = this.service.getPost(Long.valueOf(id));
        if (p == null) return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(p, HttpStatus.OK);
    }

    @DeleteMapping(path = "/posts/{id}")
    public ResponseEntity<PostResponseDto> deletePost(@PathVariable String id) throws ClassNotFoundException {
        PostResponseDto p = this.service.deletePost(Long.valueOf(id), SecurityContextHolder.getContext().getAuthentication().getName());
        if (p == null)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(p, HttpStatus.OK);

    }
}
