package com.example.civbackend.Post.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PostRequestDto {
    @Length(min = 2)
    private String title;

    @Length(min = 2)
    private String description;

    @Length(min = 2)
    private String brand;

    @Length(min = 2)
    private String model;
    
    private MultipartFile photo;
    private Long date;

    private Double longitude;

    private Double latitude;
}
