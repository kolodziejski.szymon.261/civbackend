package com.example.civbackend.Post.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostEntity {
    @Id
    @GeneratedValue
    private Long id;
    
    @NotBlank
    private String title;

    @NotBlank
    private String description;
    
    @NotBlank
    private String brand;
    
    @NotBlank
    private String model;
    
    @NotNull
    private Long userId;
    
    @NotNull
    private Long likes;

    @NotNull
    private Double longitude;

    @NotNull
    private Double latitude;

    @NotNull
    private Long date;
    
    @NotEmpty
    @Lob
    private byte[] photo;
}
