package com.example.civbackend.Post.Entity.Like;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LikeRepository extends CrudRepository<LikeEntity, Long> {
    Optional<LikeEntity> findAllByUserId(final Long userId);

    Optional<LikeEntity> findAllByPostId(final Long postId);

    Optional<LikeEntity> findAllByUserIdAndPostId(Long userId, Long postId);
}
