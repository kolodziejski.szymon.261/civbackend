package com.example.civbackend.Post.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PostResponseDto {
    private Long id;
    private String title;
    private String description;
    private String brand;
    private String model;
    private Long userId;
    private Long likes;
    private Double longitude;
    private Double latitude;
    public PostResponseDto(PostEntity post) {
        this.id=post.getId();
        this.title=post.getTitle();
        this.description=post.getDescription();
        this.brand=post.getBrand();
        this.model=post.getModel();
        this.userId=post.getUserId();
        this.likes= post.getLikes();
        this.longitude=post.getLongitude();
        this.latitude=post.getLatitude();
    }
}
