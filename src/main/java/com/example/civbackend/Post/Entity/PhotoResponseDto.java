package com.example.civbackend.Post.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PhotoResponseDto {

    private String photo;

    public PhotoResponseDto(String photo) {
        this.photo=photo;
    }
}
