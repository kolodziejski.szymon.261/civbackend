package com.example.civbackend.Post;

import com.example.civbackend.Post.Entity.Like.LikeEntity;
import com.example.civbackend.Post.Entity.Like.LikeRepository;
import com.example.civbackend.Post.Entity.Like.LikeRequestDto;
import com.example.civbackend.Post.Entity.PostEntity;
import com.example.civbackend.Post.Entity.PostRequestDto;
import com.example.civbackend.Post.Entity.PostResponseDto;
import com.example.civbackend.User.Entity.UserEntity;
import com.example.civbackend.User.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostRepository repository;
    private final UserRepository userRepository;
    private final LikeRepository likeRepository;

    @Override
    public PostResponseDto createPost(final PostRequestDto postRequestDto, String name) throws IOException {
        Long userId = userRepository.findByUsername(name).getId();
        postRequestDto.setTitle(postRequestDto.getTitle().replace("\"", ""));
        postRequestDto.setDescription(postRequestDto.getDescription().replace("\"", ""));
        postRequestDto.setBrand(postRequestDto.getBrand().replace("\"", ""));
        postRequestDto.setModel(postRequestDto.getModel().replace("\"", ""));
        final PostEntity postEntity = new PostEntity(
                null,
                postRequestDto.getTitle(),
                postRequestDto.getDescription(),
                postRequestDto.getBrand(),
                postRequestDto.getModel(),
                userId,
                0L,
                postRequestDto.getLatitude(),
                postRequestDto.getLongitude(),
                postRequestDto.getDate(),
                postRequestDto.getPhoto().getBytes()
        );
        try {
            this.repository.save(postEntity);
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.OK, "Bad request", e
            );
        }
        return new PostResponseDto(postEntity);
    }

    @Override
    public PostResponseDto likePost(LikeRequestDto rq, String name) {
        Long userId = userRepository.findByUsername(name).getId();
        Optional<PostEntity> post = repository.findById(rq.getId());
        if (post.isEmpty()) return null;

        Optional<LikeEntity> like = likeRepository.findAllByUserIdAndPostId(userId, post.get().getId());
        saveLike(userId, post.get(), like.isEmpty());
        post = repository.findById(rq.getId());
        return post.map(PostResponseDto::new).orElse(null);
    }

    @Override
    public void saveLike(Long userId, PostEntity post, boolean like) {
        if (like) post.setLikes(post.getLikes() + 1);
        else post.setLikes(post.getLikes() - 1);
        repository.save(post);
        if (!like) {
            Optional<LikeEntity> l = likeRepository.findAllByUserIdAndPostId(userId, post.getId());
            l.ifPresent(likeEntity -> likeRepository.deleteById(likeEntity.getId()));
            return;
        }
        likeRepository.save(new LikeEntity(null, userId, post.getId()));
    }

    @Override
    public PostResponseDto getPost(Long id) {
        Optional<PostEntity> user = this.repository.findById(id);
        return user.map(PostResponseDto::new).orElse(null);
    }

    @Override
    public ArrayList<PostResponseDto> getPosts() {
        ArrayList<PostResponseDto> arrayList = new ArrayList<>();
        this.repository.findAll().forEach(a -> arrayList.add(new PostResponseDto(a)));
        return arrayList;
    }

    @Override
    public byte[] getPhoto(Long id) {
        Optional<PostEntity> post = repository.findById(id);
        return post.map(PostEntity::getPhoto).orElse(null);
    }

    @Override
    public PostResponseDto deletePost(Long id, String username) {
        UserEntity user = userRepository.findByUsername(username);
        Optional<PostEntity> post = repository.findById(id);
        if (post.isEmpty() || user == null || !Objects.equals(post.get().getUserId(), user.getId()))
            return null;

        repository.delete(post.get());
        Optional<LikeEntity> l = likeRepository.findAllByPostId(post.get().getId());
        l.map(ll -> {
            likeRepository.delete(ll);
            return null;
        });
        return new PostResponseDto(post.get());
    }

    @Override
    public ArrayList<PostResponseDto> getPostsByUser(Long id) {
        Optional<ArrayList<PostEntity>> posts = repository.findAllByUserId(id);
        ArrayList<PostResponseDto> arrayList = new ArrayList<>();
        posts.ifPresent(postEntities -> postEntities.forEach(a -> arrayList.add(new PostResponseDto(a))));
        return arrayList;
    }
}
