package com.example.civbackend.Post;

import com.example.civbackend.Post.Entity.PostEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface PostRepository extends CrudRepository<PostEntity, Long> {
    @Transactional
    Optional<ArrayList<PostEntity>> findAllByUserId(final Long userId);
}