package com.example.civbackend.User;

import com.example.civbackend.Security.JwtService;
import com.example.civbackend.User.Entity.*;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.LoginException;
import java.util.ArrayList;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@Validated
public class UserController {

    private final static String BASE_URL;

    static {
//        try {
//            BASE_URL = "http://" + InetAddress.getLocalHost().getHostAddress() + ":3000";
            BASE_URL = "http://192.168.119.105:3000";
//        } catch (UnknownHostException e) {
//            throw new RuntimeException(e);
//        }
    }

    private final static long CONFIRMATION_TOKEN_EXPIRATION_TIME = 30000;
    private final static long LOGIN_TOKEN_EXPIRATION_TIME = 86400000;
    private final UserServiceImpl service;
    private final PasswordEncoder passwordEncoder;

    @PostMapping("/api/register")
    public ResponseEntity<LinkResponseDto> createUser(final @Valid @RequestBody UserRequestDto userRequestDto) {
        Optional<UserEntity> registeredUser = service.getUserByUsername(userRequestDto.getUsername());
        if (registeredUser.isPresent()) throw new DataIntegrityViolationException("");

        String password = userRequestDto.getPassword();
        String encryptedPassword = passwordEncoder.encode(password);
        userRequestDto.setPassword(encryptedPassword);
        UserEntity user = service.createUser(userRequestDto);

        String token = JwtService.generateToken(userRequestDto.getUsername(), CONFIRMATION_TOKEN_EXPIRATION_TIME);
        String confirmationLink = BASE_URL + "/api/confirm/" + token;
        System.out.println(confirmationLink);
        LinkResponseDto user2 = new LinkResponseDto(user, confirmationLink);
        return new ResponseEntity<>(
                user2,
                HttpStatus.CREATED
        );
    }


    @PostMapping("/api/login")
    public ResponseEntity<?> loginUser(final @Valid @RequestBody UserRequestDto userRequestDto) throws LoginException {
        Optional<UserEntity> registeredUser = service.getUserByUsername(userRequestDto.getUsername());
        if (registeredUser.isEmpty()) throw new LoginException("User does not exist");

        UserEntity user = registeredUser.get();
        if (!user.getConfirmed()) throw new LoginException("User is not confirmed");

        if (!passwordEncoder.matches(userRequestDto.getPassword(), user.getPassword()))
            throw new LoginException("Wrong password");

        String token = JwtService.generateToken(userRequestDto.getUsername(), LOGIN_TOKEN_EXPIRATION_TIME);
        TokenResponseDto tokenResponseDto = new TokenResponseDto(
                user.getId(),
                user.getUsername(),
                token);
        return new ResponseEntity<>(tokenResponseDto, HttpStatus.OK);
    }

    @PostMapping("/api/auth")
    public ResponseEntity<?> authenticateUser(final @RequestHeader(HttpHeaders.AUTHORIZATION) String bearer) {
        String token = bearer;
        if(bearer.startsWith("Bearer"))
            token = bearer.substring(7);
        String username = JwtService.getUsernameFromToken(token);
        if (username == null)
            return new ResponseEntity<>(new AuthResponseDto(), HttpStatus.UNAUTHORIZED);

        Optional<UserEntity> optionalUser = service.getUserByUsername(username);
        if (optionalUser.isEmpty())
            return new ResponseEntity<>(new AuthResponseDto(), HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(new AuthResponseDto(true), HttpStatus.OK);
    }


    @GetMapping("/api/confirm/{token}")
    public ResponseEntity<?> confirmUser(@PathVariable String token) {
        String username = JwtService.getUsernameFromToken(token);
        if (username == null)
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);


        Optional<UserEntity> optionalUser = service.getUserByUsername(username);
        if (optionalUser.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        UserEntity user = optionalUser.get();
        if (user.getConfirmed())
            return new ResponseEntity<>("Konto wcześniej potwierdzone", HttpStatus.OK);

        user.setConfirmed(true);
        return new ResponseEntity<>(service.updateUser(user), HttpStatus.OK);
    }

    @GetMapping("/users")
    public ResponseEntity<ArrayList<UserResponseDto>> getUsers() {
        return new ResponseEntity<>(this.service.getUsers(), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<UserResponseDto> getUsers(@PathVariable String id) {
        UserResponseDto u = this.service.getUser(Long.valueOf(id));
        if (u == null)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(u, HttpStatus.OK);
    }
}