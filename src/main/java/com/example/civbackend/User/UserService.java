package com.example.civbackend.User;

import com.example.civbackend.User.Entity.UserEntity;
import com.example.civbackend.User.Entity.UserRequestDto;
import com.example.civbackend.User.Entity.UserResponseDto;

import java.util.ArrayList;
import java.util.Optional;

public interface UserService {
    UserEntity createUser(final UserRequestDto rq);

    Optional<UserEntity> getUserByUsername(String username);
    
    ArrayList<UserResponseDto> getUsers();

    UserResponseDto getUser(Long id);
    
    UserResponseDto updateUser(UserEntity user);
}

