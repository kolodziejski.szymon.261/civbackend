package com.example.civbackend.User;

import com.example.civbackend.User.Entity.UserEntity;
import com.example.civbackend.User.Entity.UserRequestDto;
import com.example.civbackend.User.Entity.UserResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;


    @Override
    public UserEntity createUser(final UserRequestDto userRequestDTO) {
        final UserEntity userEntity = new UserEntity(
                null,
                userRequestDTO.getUsername(),
                userRequestDTO.getPassword(),
                false);
        this.repository.save(userEntity);
        return userEntity;
    }
    

    @Override
    public Optional<UserEntity> getUserByUsername(String username) {
        UserEntity userEntity = repository.findByUsername(username);
        return Optional.ofNullable(userEntity);
    }

    @Override
    public ArrayList<UserResponseDto> getUsers() {
        ArrayList<UserResponseDto> arrayList = new ArrayList<>();
        this.repository.findAll().forEach(a -> arrayList.add(new UserResponseDto(a)));
        return arrayList;
    }

    @Override
    public UserResponseDto getUser(Long id) {
        Optional<UserEntity> user = this.repository.findById(id);
        return user.map(UserResponseDto::new).orElse(null);
    }

    @Override
    public UserResponseDto updateUser(UserEntity user) {
        return new UserResponseDto(this.repository.save(user));
    }
}

