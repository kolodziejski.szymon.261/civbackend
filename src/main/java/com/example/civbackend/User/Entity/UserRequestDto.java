package com.example.civbackend.User.Entity;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRequestDto {
    @Length(min = 4)
    private String username;

    @Size(min = 3, max = 10, message = "Hasło musi mieć od 3 do 10 znaków")
    private String password;
}
