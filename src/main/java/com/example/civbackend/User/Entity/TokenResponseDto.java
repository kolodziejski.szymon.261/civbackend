package com.example.civbackend.User.Entity;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TokenResponseDto {
    private Long id;
    private String username;
    private String token;
}
