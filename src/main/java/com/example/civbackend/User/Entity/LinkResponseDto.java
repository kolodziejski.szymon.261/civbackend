package com.example.civbackend.User.Entity;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LinkResponseDto {
    private Long id;
    private String username;
    private String link;


    public LinkResponseDto(UserEntity user, String confirmationLink) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.link = confirmationLink;
    }
}
