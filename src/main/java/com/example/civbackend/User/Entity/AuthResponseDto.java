package com.example.civbackend.User.Entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class AuthResponseDto {
    private boolean authenticated;

    public AuthResponseDto() {
        this.authenticated = false;
    }
}
