package com.example.civbackend.User.Entity;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserResponseDto {
    private Long id;
    private String username;
    public UserResponseDto(UserEntity user) {
        this.id = user.getId();
        this.username = user.getUsername();
    }
}
